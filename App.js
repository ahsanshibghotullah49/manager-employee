import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';
import People from './src/image/People.png';

class App extends Component {
  render() {
    return (
      <View>
        <Text>Icon People</Text>
        <Image source={People} style={{ width: 50, height: 50 }} />
      </View>
    );
  }
}

export default App;
